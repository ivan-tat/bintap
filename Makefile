#!/bin/make -f
#
# Supported environments:
#   * GNU on Linux, FreeBSD etc.
#   * GNU on Windows NT (using MinGW/MSYS/Cygwin/WSL)
#
# Build:
#   make [options]
# Install / Uninstall:
#   make [options] install | uninstall
# Clean:
#   make [options] clean | distclean
#
# Options:
#   BUILD=<BUILD>
#     <BUILD> - see included `common.mak'.
#   prefix=<PREFIX>
#     <PREFIX> is a prefix directory to install files into.
#
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

include common.mak

builddir	=build
prefix		?=/usr/local
exec_prefix	?=$(prefix)
bindir		?=$(exec_prefix)/bin
libdir		?=$(exec_prefix)/lib

VERSION		:=$(shell cat VERSION)

LINUX_I686	=bintap-$(VERSION)-linux-i686.tar.bz2
LINUX_AMD64	=bintap-$(VERSION)-linux-amd64.tar.bz2
WINDOWS_I686	=bintap-$(VERSION)-windows-i686.zip
WINDOWS_AMD64	=bintap-$(VERSION)-windows-amd64.zip

.PHONY: all
all: build-bintap

build \
build/linux-i686 \
build/linux-amd64 \
build/windows-i686 \
build/windows-amd64 \
$(DESTDIR)$(bindir) \
$(DESTDIR)$(libdir):
	$(MKDIR) -p $@

#-----------------------------------------------------------------------------
# bintap

.PHONY: build-bintap
build-bintap: | src
	$(MAKE) -w -C $| prefix=../$(builddir) install

$(DESTDIR)$(bindir)/%$E: $(builddir)/bin/%$E | $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) $< $@

.PHONY: install-bintap
install-bintap: $(DESTDIR)$(bindir)/bintap$E

.PHONY: uninstall-bintap
uninstall-bintap:
	$(RM) $(DESTDIR)$(bindir)/bintap$E

.PHONY: clean-built-bintap
clean-built-bintap:
	$(RM) $(builddir)/bin/bintap$E

.PHONY: clean-bintap
clean-bintap: clean-built-bintap | src
	$(MAKE) -w -C $| clean

.PHONY: distclean-bintap
distclean-bintap: clean-built-bintap | src
	$(MAKE) -w -C $| distclean

#-----------------------------------------------------------------------------
# Release: Linux

ifneq ($(RELEASE_LINUX),)
build/$(RELEASE_LINUX): | src
	$(MAKE) -w -C $|\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(RM) $@ && cd build/$(RELEASE_SUBDIR) &&\
	 tar --owner= --group= -cjf ../$(@F)\
	 bin/bintap
endif

#-----------------------------------------------------------------------------
# Release: Windows

ifneq ($(RELEASE_WINDOWS),)
build/$(RELEASE_WINDOWS): | src
	$(MAKE) -w -C $|\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(RM) $@ && cd build/$(RELEASE_SUBDIR) &&\
	 zip -r9 ../$(@F)\
	 bin/bintap.exe
endif

#-----------------------------------------------------------------------------
# Release

.PHONY: release
release: | build
	echo -n '' >build/release.md
 ifneq ($(OS),Windows_NT)
	{ echo '**GNU/Linux x86** platform:';\
	 echo '';\
	 echo 'Architecture | File | SHA-256';\
	 echo '---|---|---'; } >>build/release.md

	$(MAKE) -w CFLAGS='-m32 $(CFLAGS)'\
	 RELEASE_SUBDIR=linux-i686\
	 RELEASE_LINUX=$(LINUX_I686)\
	 build/$(LINUX_I686)
	{ echo -n '32-bits (i686) | $(LINUX_I686) | ';\
	 sha256sum build/$(LINUX_I686) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w CFLAGS='-m64 $(CFLAGS)'\
	 RELEASE_SUBDIR=linux-amd64\
	 RELEASE_LINUX=$(LINUX_AMD64)\
	 build/$(LINUX_AMD64)
	{ echo -n '64-bits (AMD64) | $(LINUX_AMD64) | ';\
	 sha256sum build/$(LINUX_AMD64) | cut -d\  -f 1; } >>build/release.md
	echo ''>> build/release.md
 endif
	{ echo '**Windows x86** platform:';\
	 echo '';\
	 echo 'Architecture | File | SHA-256';\
	 echo '---|---|---'; } >>build/release.md
	$(MAKE) -w BUILD=mingw32\
	 RELEASE_SUBDIR=windows-i686\
	 RELEASE_WINDOWS=$(WINDOWS_I686)\
	 build/$(WINDOWS_I686)
	{ echo -n '32-bits (i686) | $(WINDOWS_I686) | ';\
	 sha256sum build/$(WINDOWS_I686) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w BUILD=mingw64\
	 RELEASE_SUBDIR=windows-amd64\
	 RELEASE_WINDOWS=$(WINDOWS_AMD64)\
	 build/$(WINDOWS_AMD64)
	{ echo -n '64-bits (AMD64) | $(WINDOWS_AMD64) | ';\
	 sha256sum build/$(WINDOWS_AMD64) | cut -d\  -f 1; } >>build/release.md

.PHONY: clean-built-release
clean-built-release:
	$(RM) build/release.md
 ifneq ($(OS),Windows_NT)
	$(RM) -r build/linux-i686
	$(RM) build/$(LINUX_I686)
	$(RM) -r build/linux-amd64
	$(RM) build/$(LINUX_AMD64)
 endif
	$(RM) -r build/windows-i686
	$(RM) build/$(WINDOWS_I686)
	$(RM) -r build/windows-amd64
	$(RM) build/$(WINDOWS_AMD64)

.PHONY: clean-release
clean-release: clean-built-release | src
 ifneq ($(OS),Windows_NT)
	$(MAKE) -w -C $| builddir=build/linux-i686 clean
	$(MAKE) -w -C $| builddir=build/linux-amd64 clean
 endif
	$(MAKE) -w -C $| BUILD=mingw32 builddir=build/windows-i686 clean
	$(MAKE) -w -C $| BUILD=mingw64 builddir=build/windows-amd64 clean

.PHONY: distclean-release
distclean-release: clean-built-release | src
 ifneq ($(OS),Windows_NT)
	$(MAKE) -w -C $| builddir=build/linux-i686 distclean
	$(MAKE) -w -C $| builddir=build/linux-amd64 distclean
 endif
	$(MAKE) -w -C $| BUILD=mingw32 builddir=build/windows-i686 distclean
	$(MAKE) -w -C $| BUILD=mingw64 builddir=build/windows-amd64 distclean

#-----------------------------------------------------------------------------
# Common: install, install-strip, uninstall, clean, distclean

.PHONY: install
install: install-bintap

.PHONY: install-strip
install-strip:
	$(MAKE) -w INSTALL_PROGRAM='$(INSTALL_PROGRAM) -s' install

.PHONY: uninstall
uninstall: uninstall-bintap

.PHONY: clean
clean: clean-bintap

.PHONY: distclean
distclean: distclean-bintap
	$(RM) -r build
