/*
 * opts.h - declarations for `opts.c'.
 *
 * This file is a part of `bintap' program.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2020-2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#ifndef _opts_h_included
#define _opts_h_included 1

/* String value is passed in `optarg' variable (see `getopt.h'). */
struct setopt_param_t {
    char long_form;
    const char *name;
    void *var;
    int flag;
};

/* Terminate the list of options with an element filled with zeros. */
struct ext_option_t {
    char short_name;    /* '\0' for none */
    char *long_name;    /* NULL for none */
    char has_arg;       /* passed to `getopt_long()' */
    int (*proc) (struct setopt_param_t *);
    void *var;          /* reference to a modifiable variable or NULL */
    int flag;           /* +`var' are passed as parameters to `setopt_*()' functions */
};
/* If `proc' is 0 and `var' is not 0 then `var' is assumed to be a reference to a
   variable of type `int' and recieves the value stored in `flag' (in the same way
   getopt_long() does it). */

char optval_long_int (char long_form, const char *opt_name, char *str, long *val, long min, long max);
char optval_char (char long_form, const char *opt_name, char *str, char *val, int min, int max);
char optval_uint (char long_form, const char *opt_name, char *str, unsigned *val, unsigned min, unsigned max);

char init_opts (const struct ext_option_t *options, char **shortopts, struct option **longopts);
int find_short_opt (const struct ext_option_t *options, char name);
int find_long_opt (const struct ext_option_t *options, const char *name);
int set_opt (const struct ext_option_t *options, char long_form, const char *name, int ind);
void free_opts (char **shortopts, struct option **longopts);

#endif  /* !_opts_h_included */
