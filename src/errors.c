/*
 * errors.c - routines to print common error messages.
 *
 * This file is a part of `bintap' program.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2020-2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#include <stdio.h>
#include "errors.h"

void message (const char *format, ...) {
  va_list ap;

  va_start (ap, format);
  fprintf (stderr, PROGRAM ": ");
  vfprintf (stderr, format, ap);
  fprintf (stderr, "\n");
  va_end (ap);
}

void warn (const char *format, ...) {
  va_list ap;

  va_start (ap, format);
  fprintf (stderr, PROGRAM ": Warning: ");
  vfprintf (stderr, format, ap);
  fprintf (stderr, "\n");
  va_end (ap);
}

void error (const char *format, ...) {
  va_list ap;

  va_start (ap, format);
  fprintf (stderr, PROGRAM ": ERROR: ");
  vfprintf (stderr, format, ap);
  fprintf (stderr, "\n");
  va_end (ap);
}
