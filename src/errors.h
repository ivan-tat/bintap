/*
 * errors.h - declarations for `errors.c'.
 *
 * This file is a part of `bintap' program.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2020-2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#ifndef _errors_h_included
#define _errors_h_included

#include <stdarg.h>

void message (const char *format, ...);
void warn (const char *format, ...);
void error (const char *format, ...);

#endif  /* !_errors_h_included */
