/*
 * version.h - main program version number.
 *
 * This file is a part of `bintap' program.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: 2020-2023 Ivan Tatarinov
 * SPDX-License-Identifier: Unlicense
 */

#ifndef _version_h_included
#define _version_h_included 1

#define VERSION "1.0"

#endif  /* !_version_h_included */
