# bintap

Binary files to ZX Spectrum *.tap* tape file converter to be used in ZX Spectrum emulating software.

## License

Public domain (see [Unlicense.txt](LICENSES/Unlicense.txt) file).

## Authors

Main developer: Ivan Tatarinov, <ivan-tat at ya dot ru>

## Build using GNU/Linux environment

### Compile

```sh
cd src
make
```

### Install

As *root* or using `sudo`:

```sh
make install
```

or

```sh
make install-strip
```

### Uninstall

As *root* or using `sudo`:

```sh
make uninstall
```

### Clean

```sh
make clean
```

or

```sh
make distclean
```

## Usage

```
Usage: bintap [OPTIONS] INPUT_FILE

Options:
  -h, --help                            show this help and exit.
      --version                         show version and exit.
  -p, --program                         make `Program' instead of `Bytes'.
  -t TITLE, --title TITLE               set header name for all blocks.
  -s LINE, --start-line LINE            BASIC start line for program.
  -o FILENAME, --output FILENAME        set output filename.
      --auto-name                       make output filename from input.
  -a, --append                          append tape at end of file.
  -l ADDRESS, --load-address ADDRESS    load address of a binary file.
  -x ADDRESS, --extra-address ADDRESS   extra address of a binary file.

BASIC loader options:
  -b, --basic                           include BASIC loader.
  -d, --d80                             create D80 syntax loader.
  -c ADDRESS, --clear-address ADDRESS   set clear address.
  -e ADDRESS, --exec-address ADDRESS    set code start address.
      --bc COLOR, --border-color COLOR  set border color.
      --pc COLOR, --paper-color COLOR   set paper color.
      --ic COLOR, --ink-color COLOR     set ink color.
      --nph, --no-print-headers         hide header title when loading.

Maximum supported input file size is 49152 bytes.
Maximum `TITLE' length is 10.
`LINE' is a number in range [0; 9999].
`ADDRESS' is a number in range [0; 65535].
`COLOR' is a number in range [0; 7].
All numbers are decimal or hexadecimal (prefixed with `0x' or `0X').
```

## References

* [REUSE SOFTWARE](https://reuse.software/) - a set of recommendations to make licensing your Free Software projects easier
* [REUSE tool](https://reuse.software/) - tool for REUSE copyright and license recommendations ([package](https://pkgs.org/download/reuse))
* [The Software Package Data Exchange (SPDX)](https://spdx.dev/) - An open standard for communicating software bill of material information, including components, licenses, copyrights, and security references
* [GNU Operating System](https://www.gnu.org/)
* [GNU Core Utilities](https://www.gnu.org/software/coreutils/) ([package](https://pkgs.org/download/coreutils))
* [GNU Bash](https://www.gnu.org/software/bash/) - GNU Bourne Again SHell ([package](https://pkgs.org/download/bash))
* [GNU Make](https://www.gnu.org/software/make/) - utility for directing compilation ([package](https://pkgs.org/download/make))
* [GNU Compiler Collection](https://www.gnu.org/software/gcc/) ([package](https://pkgs.org/download/gcc))
* [GNU Standards](http://savannah.gnu.org/projects/gnustandards) - GNU coding and package maintenance standards ([package](https://pkgs.org/download/gnu-standards))
* [Cygwin](https://cygwin.com/) - a large collection of GNU and Open Source tools which provide functionality similar to a Linux distribution on Windows
* [MSYS2](https://www.msys2.org/) - a collection of tools and libraries providing you with an easy-to-use environment for building, installing and running native Windows software
* [MinGW](https://osdn.net/projects/mingw/) - Minimalist GNU for Windows
* [MinGW-w64](https://www.mingw-w64.org/) - an advancement of the original mingw.org project, created to support the GCC compiler on Windows systems
* [WSL](https://learn.microsoft.com/en-us/windows/wsl/) - Windows Subsystem for Linux Documentation
* [Description of TAP file format on faqwiki.zxnet.co.uk](https://faqwiki.zxnet.co.uk/wiki/TAP_format)
* [ZX-Spectrum utilities](https://zxspectrumutils.sourceforge.io/) - ZX-Spectrum emulators format utilities.
