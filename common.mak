#!/bin/make -f
#
# common.mak - common definitions for makefiles
#
# Supported environments:
#   * GNU on Linux, FreeBSD etc.
#   * GNU on Windows NT (using MinGW/MSYS/Cygwin/WSL)
#
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

ifeq ($(BUILD),mingw32)
 CC = i686-w64-mingw32-gcc
else ifeq ($(BUILD),mingw64)
 CC = x86_64-w64-mingw32-gcc
else ifneq ($(BUILD),)
 $(error Unknown build: "$(BUILD)")
endif

# Check value of RELEASE
ifeq ($(RELEASE),linux)
else ifeq ($(RELEASE),windows)
else ifneq ($(RELEASE),)
 $(error Unknown release: "$(RELEASE)")
endif

# E=executable file suffix (including leading dot)

ifeq ($(OS),Windows_NT)
 ifeq ($(RELEASE),linux)
  E=
 else
  E=.exe
 endif
else ifeq ($(BUILD),mingw32)
 E=.exe
else ifeq ($(BUILD),mingw64)
 E=.exe
else ifeq ($(shell uname -s),Linux)
 ifeq ($(RELEASE),windows)
  E=.exe
 else
  E=
 endif
else ifneq ($(DJGPP),)
 $(warn Not fully tested)
 E=.exe
else
 $(error Unknown platform)
endif

INSTALL		?=install
INSTALL_PROGRAM	?=$(INSTALL)
INSTALL_DATA	?=$(INSTALL) -m 644
MKDIR		?=mkdir
RM		?=rm -f
